#!/bin/bash

if [ -z "$1" ]; then
    echo "Please specify index!"
    echo
    exit 1
fi

index_name="$1"
es_host="127.0.0.1:9200"

response=`curl -is "$es_host/$index_name"`
status=`echo "$response" | head -n1 | cut -d' ' -f2`

if [ "$status" = 404 ]; then
    echo "Index does not exist"
    echo
    exit 0
elif [ ! "$status" = 200 ]; then
    echo "Error"
    echo
    exit 0
fi

curl -X DELETE "$es_host/$index_name"
echo
